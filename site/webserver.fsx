// --------------------------------------------------------------------------------------
// Start up Suave.io
// --------------------------------------------------------------------------------------

#r "../packages/FAKE/tools/FakeLib.dll"
#r "../packages/Suave/lib/net40/Suave.dll"

(*
open Fake
open Suave
open Suave.Http.Successful
open Suave.Http
open Suave.Http.Successful
open Suave.Http.Redirection
open Suave.Http.Files
open Suave.Http.RequestErrors
open Suave.Http.Applicatives
open Suave.Web
open Suave.Types
open System.Net

let app =
  choose
    [ GET >=> choose
        [ path "/hello" >=> OK "Hello GET"
          path "/goodbye" >=> OK "Good bye GET" ]
      POST >=> choose
        [ path "/hello" >=> OK "Hello POST"
          path "/goodbye" >=> OK "Good bye POST" ] ]

let serverConfig = 
    let port = getBuildParamOrDefault "port" "8083" |> Sockets.Port.Parse
    { defaultConfig with bindings = [ HttpBinding.mk HTTP IPAddress.Loopback port ] }

startWebServer serverConfig app

    (OK  
        ("Hello World! It's Suave.io on Azure Websites.<br /><ul>" + 
          "<li>Sample git Repo: <a href='https://github.com/shanselman/suavebootstrapper'>https://github.com/shanselman/suavebootstrapper</a></li>" +
          "<li>Intro blog post: <a href='http://www.hanselman.com/blog/RunningSuaveioAndFWithFAKEInAzureWebAppsWithGitAndTheDeployButton.aspx'>http://www.hanselman.com/blog/RunningSuaveioAndFWithFAKEInAzureWebAppsWithGitAndTheDeployButton.aspx</a></li>" +
          "</ul>")) *)


open Suave
open Suave.Web
open Suave.Http
open Suave.Types
open Suave.Http.Successful
open Suave.Http.Redirection
open Suave.Http.Files
open Suave.Http.RequestErrors
open Suave.Http.Applicatives

let mutable id = 0
let mutable todos = []

let getTodos () =
    todos
    |> List.map (fun t -> sprintf "{ \"id\": %d, \"text\": \"%s\" }" (fst t) (snd t))
    |> String.concat ","
    |> sprintf "{ \"todos\": [ %s ] }" 

let add text =
    match text with
    | Some(t) -> 
        let next = id + 1
        id    <- next
        todos <- (id, t) :: todos
        ()
    | None -> 
        ()

let remove id = 
    let removed =
        todos
        |> List.filter (fun t -> (fst t) <> id)
    todos <- removed
    ()

let app : WebPart =
    choose 
        [ GET >>= choose
            [ path "/" >>= OK "<h3> FUCK YOU! </h3>";
              path "/todos" >>= request (fun req -> OK (getTodos ())) ]   
          POST >>= choose
            [ path "/todos" >>= request (fun req -> add (req.formData "text") ; OK "") ]
          DELETE >>= choose
            [ pathScan "/todos/%d" (fun (id) -> remove id ; OK "") ]       
        ]